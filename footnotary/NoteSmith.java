package footnotary;

import java.io.*;

public class NoteSmith
{
	public static void main(String[] args)
	{
		if( args.length < 2 )
		{
			System.out.println("Usage: java NoteSmith inputFilename outputFilename");
		}
		else
		{
			String inputFilename = args[0];
			String outputFilename = args[1];
			String inputText = readTextFile(inputFilename);
			
			// process the text
			String outputText = FootnoteMaker.process(inputText);
			
			boolean outputResult = writeTextFile(outputFilename, outputText);
		}
	}
	
	public static String readTextFile(String path)
	{
		try
		{
			File file = new File(path);
			FileInputStream fis = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			fis.read(data);
			fis.close();
			return new String(data, "UTF-8");
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
			return "";
		}
	}
	
	public static boolean writeTextFile(String path, String content)
	{
		try
		{
			File file = new File(path);
            BufferedWriter output = new BufferedWriter(new FileWriter(file));
            output.write(content);
			output.close();
			return true;
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
			return false;
		}
	}
}

// Take a string as input.
// Replace all instances of [Footnote XXXXX] with a numbered html kindle style footnote.
// Put the footnote text in the immediately following [Footnotes] section.
class FootnoteMaker
{
	public static String process(String input)
	{
		String output = input;
		String footnoteText = ""; // the accumulated footnotes to go in [Footnotes]
		int currentFootnote = 1; // the running count of footnotes
		
		KindleNoteRenderer noteRenderer = new KindleNoteRenderer();
		
		while(output.indexOf("[Footnote ") >= 0)
		{
			// get the position of the next footnote
			int footnoteStartIndex = output.indexOf("[Footnote ");
			int footnoteEndIndex = output.indexOf("]", footnoteStartIndex);
			
			// get the text of the next footnote
			String newFootnoteText = output.substring(footnoteStartIndex + 10, footnoteEndIndex);
			
			// if there is a new footnote
			if( newFootnoteText.length() > 0 )
			{
				footnoteText += noteRenderer.getFootnote(currentFootnote, newFootnoteText);
				
				String textNumber = noteRenderer.getNoteWithinText(currentFootnote);
				
				// replace the footnote with a number
				output = output.substring(0, footnoteStartIndex) + textNumber + output.substring(footnoteEndIndex + 1, output.length());
			
				// increment the footnote count
				currentFootnote++;
			}
			
			// re-asses the destination position of the new footnote, which changed when we inserted the number
			int footnotesDestinationPosition = output.indexOf("[Footnotes]", 0); // this footnote goes at the first appearance of [Footnotes]
			int nextFootnotesDestinationPosition = output.indexOf("[Footnotes]", output.indexOf("[Footnote ")); // where does the next foootnote go?
			
			// if we are moving on to the next batch of footnotes
			if( nextFootnotesDestinationPosition != footnotesDestinationPosition )
			{
				// insert all the footnotes at the right position
				output = output.substring(0, footnotesDestinationPosition) + footnoteText + output.substring(footnotesDestinationPosition + 11, output.length());
			
				
				// reset the footnote String
				footnoteText = "";
			}
		}
		
		// if there is a remaining footnote section
		if( footnoteText.length() > 0 )
		{
			int footnotesDestinationPosition = output.indexOf("[Footnotes]", 0); // this footnote goes at the first appearance of [Footnotes]
				
			if( footnotesDestinationPosition > -1 )
			{
				// insert the last batch of footnotes at the right position
				output = output.substring(0, footnotesDestinationPosition) + footnoteText + output.substring(footnotesDestinationPosition + 11, output.length() - 1);
			}
		}
	
		// remove all the [Footnotes] tags
		output = output.replace("[Footnotes]", "");
		
		return output;
	}
}