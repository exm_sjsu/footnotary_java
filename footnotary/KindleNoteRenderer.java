package footnotary;

//import footnotary.NoteRenderer;

// An implementation of the NoteRenderer interface for kindle html.
public class KindleNoteRenderer implements NoteRenderer
{
	public KindleNoteRenderer()
	{
		// Nothing here at the moment.
	}
	
	// Get the text/markup that will be inserted at the position of the footnote within the body of the text.
	public String getNoteWithinText(int noteNumber)
	{
		return "<a href=\"#footnote-" + noteNumber + "\" name=\"footnote-" + noteNumber + "-ref\"><sup>" + noteNumber + "</sup></a>";	
	}
	
	// Get the text/markup that will be inserted at the end of the chapter or wherever the note will be rendered.
	public String getFootnote(int noteNumber, String noteText)
	{
		// make the numeric footnote link that goes in the footnote
		String footnoteNumber = "<a href=\"#footnote-" + noteNumber + "-ref\" name=\"footnote-" + noteNumber + "\">" + noteNumber + "</a>. ";
				
		// put the new footnote text in the running batch of footnotes
		return "<br/>" + footnoteNumber + noteText;
	}
}
