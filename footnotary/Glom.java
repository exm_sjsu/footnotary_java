package footnotary;

import java.io.*;

// What's the purpose of this class? It gloms all the text files in a directory together.
public class Glom
{
	public static void main(String[] args)
	{
		if( args.length < 2 )
		{
			System.out.println("Usage: java Glom inputDirectory outputFilename");
		}
		else
		{
			String inputDirectory = args[0];
			String outputFilename = args[1];
			
			File folder = new File(inputDirectory);
			File[] listOfFiles = folder.listFiles();

			String outputText = "";
			for (File file : listOfFiles) {
				if (file.isFile()) {
					outputText += readTextFile(file.getAbsolutePath());
				}
			}
			
			boolean outputResult = writeTextFile(outputFilename, outputText);
		}
	}
	
	public static String readTextFile(String path)
	{
		try
		{
			File file = new File(path);
			FileInputStream fis = new FileInputStream(file);
			byte[] data = new byte[(int) file.length()];
			fis.read(data);
			fis.close();
			return new String(data, "UTF-8");
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
			return "";
		}
	}
	
	public static boolean writeTextFile(String path, String content)
	{
		try
		{
			File file = new File(path);
            BufferedWriter output = new BufferedWriter(new FileWriter(file));
            output.write(content);
			output.close();
			return true;
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
			return false;
		}
	}
}
