package footnotary;

// The NoteRenderer interface defines methods for rendering footnotes in a specific format.
public interface NoteRenderer
{
	// Get the text/markup that will be inserted at the position of the footnote within the body of the text.
	public String getNoteWithinText(int noteNumber);
	
	// Get the text/markup that will be inserted at the end of the chapter or wherever the note will be rendered.
	public String getFootnote(int noteNumber, String noteText);
}
